package com.epam.lab.accounts.accounts.facade;

import com.epam.lab.accounts.accounts.dto.AccountDTO;
import com.epam.lab.accounts.accounts.model.requests.CreateUpdateAccountRequest;
import com.epam.lab.accounts.accounts.service.AccountService;
import com.epam.lab.accounts.accounts.service.ErrorsService;
import com.epam.lab.accounts.accounts.service.SessionService;
import com.epam.lab.accounts.accounts.validator.CreateAccountRequestRequestValidator;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import java.math.BigDecimal;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountFacadeTest {
    @Mock
    private AccountService accountService;

    private AccountFacade accountFacade = new AccountFacade();
    private CreateAccountRequestRequestValidator requestValidator = new CreateAccountRequestRequestValidator();
    private ErrorsService errorsService = new ErrorsService();
    private MockHttpSession httpSession = new MockHttpSession();

    @Mock
    private SessionService sessionService;

    @Before
    public void resetSession() {
        ReflectionTestUtils.setField(errorsService, "sessionService", sessionService);
        ReflectionTestUtils.setField(accountFacade, "sessionService", sessionService);
        ReflectionTestUtils.setField(accountFacade, "accountService", accountService);
        ReflectionTestUtils.setField(requestValidator, "errorsService", errorsService);
        ReflectionTestUtils.setField(requestValidator, "accountService", accountService);
        ReflectionTestUtils.setField(accountFacade, "createAccountRequestValidator", requestValidator);
        ReflectionTestUtils.setField(sessionService, "session", httpSession);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateAccountForCurrentUser() {
        final AccountDTO accountDTO = getAccountDefaultForSession() ;
        final CreateUpdateAccountRequest createAccountRequest = getCreateAccountRequest();
        when(accountService.isAccountExistsForAccountCode(createAccountRequest.getAccountCode())).thenReturn(false);
        accountFacade.handleCreateOrUpdateAccountRequest(createAccountRequest);
        verify(accountService).createAccountForCurrentUser(accountDTO);
    }
    @Test
    public void testIsAccountExistCodeFailed(){
        final AccountDTO accountDTO = getAccountDefaultForSession();
        when(accountService.isAccountExistsForAccountCode(accountDTO.getCode())).thenReturn(false);
    }

    @Test
    public void testIsAccountBelongsForCurrentUser() {
        final CreateUpdateAccountRequest createUpdateAccountRequest = getCreateAccountRequest();
        when(accountService.isAccountExistsForAccountCode(getCreateAccountRequest().getAccountCode())).thenReturn(true);
    }

    @Test
    public void testIsAccountNotBelongsForCurrentUser() {
        final CreateUpdateAccountRequest createUpdateAccountRequest = getCreateAccountRequest();
        when(accountService.isAccountExistsForAccountCode(createUpdateAccountRequest.getAccountCode())).thenReturn(false);
        accountFacade.handleCreateOrUpdateAccountRequest(createUpdateAccountRequest);
    }



//    @Test
//    public void testHandleCreateOrUpdateAccountRequestFailed() {
//        final CreateUpdateAccountRequest createUpdateAccountRequest = getCreateAccountRequest();
//        //WHEN
//        when(accountFacade.handleCreateOrUpdateAccountRequest(createUpdateAccountReques));
//    }
//


    private AccountDTO getAccountDefaultForSession() {
        final AccountDTO accountDTO = new AccountDTO();
        BigDecimal balance = new BigDecimal(1000.0);
        accountDTO.setCode("adidas-ftw ");
        accountDTO.setName("Adidas Footwear department");
        accountDTO.setImg("http://pngimg.com/uploads/adidas/adidas_PNG18.png");
        accountDTO.setBalance(balance);
        return accountDTO;
    }

    private AccountDTO getDefaultAccountForRequest() {
        final AccountDTO accountDTO =  new AccountDTO();
        BigDecimal balance = new BigDecimal(1500.00);
        accountDTO.setCode(" adidas ");
        accountDTO.setName("Adidas Footwear department 2");
        accountDTO.setImg("http://pngimg.com/uploads/adidas/adidas_PNG18.png");
        accountDTO.setBalance(balance);
        return accountDTO;
    }

    private CreateUpdateAccountRequest getCreateAccountRequest() {
        final AccountDTO accountDTO = getDefaultAccountForRequest();
        return getCreateUpdateAccountRequest(accountDTO);
    }

    private CreateUpdateAccountRequest getCreateNewAccountRequest() {
        final AccountDTO accountDTO = getDefaultAccountForRequest() ;
        return getCreateUpdateAccountRequest(accountDTO);
    }

    private CreateUpdateAccountRequest getCreateUpdateAccountRequest(AccountDTO accountDTO) {
        final CreateUpdateAccountRequest createAccountRequest = new CreateUpdateAccountRequest();
        createAccountRequest.setAccountCode(accountDTO.getCode());
        createAccountRequest.setAccountName(accountDTO.getName());
        createAccountRequest.setAccountBalance(accountDTO.getBalance());
        createAccountRequest.setAccountImage(accountDTO.getImg());
        return createAccountRequest;
    }
    private CreateUpdateAccountRequest getCreatedAccount() {
        final AccountDTO accountDTO = getAccountDefaultForSession();
        final CreateUpdateAccountRequest createAccountRequest = new CreateUpdateAccountRequest();
        createAccountRequest.setAccountCode(accountDTO.getCode());
        return createAccountRequest;
    }

  private CreateUpdateAccountRequest getDefaultUpdateAccountRequest() {
      final AccountDTO accountDTO = getDefaultAccountForRequest();
      final CreateUpdateAccountRequest updateAccountRequest = new CreateUpdateAccountRequest();
      updateAccountRequest.setAccountCode(accountDTO.getCode());
      updateAccountRequest.setAccountBalance(accountDTO.getBalance());
      updateAccountRequest.setAccountName(accountDTO.getName());
      updateAccountRequest.setAccountImage("https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Nokia_Lumia_logo.svg/1280px-Nokia_Lumia_logo.svg.png");
      return updateAccountRequest;
  }

}
